import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import List from '@/components/List'
import Add from '@/components/Add'

Vue.use(Router)
export default new Router({
    routes: [
        {
            path: '/',
            name: 'Root',
            component: Home
        },
        {
            path: '/list',
            name: 'List',
            component: List
        },
        {
            path: '/peserta/tambah',
            name: 'add',
            component: Add
        }
    ],
    mode: 'history'
})